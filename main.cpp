#include <iostream>
#include <ctime>

using namespace std;

int main() {
    time_t now = time(0);
    int day = localtime(&now)->tm_mday;
    int N;

    cout << "Enter N: ";
    cin >> N;
    int massiv[N][N];
    for(int i=0; i<N; i++){
        cout << "\n";
        for(int j=0; j<N; j++){
            massiv[i][j] = i+j;
            cout << massiv[i][j] << " ";
        }
    }
    int lineNumber = day%N, sum=0;
    for(int i=0; i<N; i++){
        sum += massiv[lineNumber][i];
    }
    cout << "\nSumma " << lineNumber << " stroki = " << sum;
    return 0;
}